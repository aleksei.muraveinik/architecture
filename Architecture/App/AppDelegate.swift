//
//  AppDelegate.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import SwiftUI

typealias LaunchOptions = [UIApplication.LaunchOptionsKey: Any]

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: Properties
    let window: UIWindow! = UIWindow()
    private let appContainer = AppContainer()
    private lazy var deeplinkService = appContainer.deeplinkService
    
    
    // MARK: Lifecycle
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: LaunchOptions?
    ) -> Bool {
        launch()
        return true
    }
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        guard let host = url.host, let screen = RootScreen.fromString(host) else { return false }
        
        deeplinkService.setTarget(screen)
        
        return true
    }
    
    
    // MARK: Private
    private func launch() {
        window.rootViewController = appContainer.makeRootCoordinator()
        window.makeKeyAndVisible()
    }
}


private extension RootScreen {
    static func fromString(_ string: String) -> RootScreen? {
        switch string {
        case "root":
            return .root
        case "auth":
            return .auth
        case "main":
            return .main
        default:
            return nil
        }
    }
}
