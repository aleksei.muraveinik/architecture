//
//  View+UIViewController.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import SwiftUI

extension View {
    var controller: UIHostingController<Self> { UIHostingController(rootView: self) }
}
