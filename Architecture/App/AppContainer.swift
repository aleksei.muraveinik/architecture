//
//  AppContainer.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import UIKit

final class AppContainer {
    lazy var deeplinkService = DeeplinkServiceImpl(router: rootRouter)
    private let rootRouter = Router(RootScreen.graph)
}


// MARK: Interface
extension AppContainer {
    func makeRootCoordinator() -> UIViewController {
        Coordinator<RootScreen>(
            container: RootContainer(
                router: rootRouter,
                deeplinkService: deeplinkService
            )
            .eraseToAnyCoordinatorContainer()
        )
    }
}
