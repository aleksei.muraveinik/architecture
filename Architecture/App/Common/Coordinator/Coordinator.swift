//
//  Coordinator.swift
//  Architecture
//
//  Created by Apple on 29.09.22.
//

import Combine
import UIKit

class Coordinator<Screen: Routable>: UIViewController {
    
    // MARK: Properties
    private var navigationCancellable: AnyCancellable?
    private var backCancellable: AnyCancellable?
    
    private let navController: UINavigationController
    
    
    // MARK: Lifecycle
    init(
        router: Router<Screen>,
        makeController: @escaping (Screen) -> UIViewController
    ) {
        self.navController = UINavigationController(rootViewController: makeController(Screen.root))
        super.init(nibName: nil, bundle: nil)
        
        navController.navigationBar.isHidden = true
        let childView = navController.view!
        
        addChild(navController)
        view.addSubview(childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            childView.topAnchor.constraint(equalTo: view.topAnchor),
            childView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            childView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            childView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
        navController.didMove(toParent: self)
        
        
        navigationCancellable = router.routesPublisher
            .receive(on: RunLoop.main)
            .sink { [weak self] routes in
                self?.navigate(routes, makeController)
            }
    }
    
    convenience init(
        container: AnyCoordinatorContainer<Screen>
    ) {
        self.init(
            router: container.router,
            makeController: container.makeController
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: navigate
private extension Coordinator {
    func navigate<Screen>(
        _ routes: [Route<Screen>],
        _ makeController: @escaping (Screen) -> UIViewController
    ) {
        routes.reversed()
            .reduce({}) { completion, route in
                let screen = route.target
                switch route.action {
                case .dismiss:
                    return {
                        self.dismiss(animated: true, completion: completion)
                    }
                
                case .present:
                    return {
                        guard self.presentedViewController == nil else {
                            assertionFailure("Can not present because root has presented controller")
                            return
                        }
                        
                        let controller = makeController(screen)
                        controller.isModalInPresentation = true
                        self.present(
                            controller,
                            animated: true,
                            completion: completion
                        )
                    }
                    
                case .push:
                    return {
                        guard self.presentedViewController == nil else {
                            assertionFailure("Can not push because root has presented controller")
                            return
                        }
                        
                        self.navController.pushViewController(
                            makeController(screen),
                            animated: true
                        )
                        completion()
                    }
                    
                case .pop:
                    return {
                        self.navController.popViewController(animated: true)
                        completion()
                    }
                    
                case .popToRoot:
                    return {
                        self.navController.popToRootViewController(animated: true)
                        completion()
                    }
                }
            }()
    }
}
