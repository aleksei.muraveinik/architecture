//
//  CoordinatorContainer.swift
//  Architecture
//
//  Created by Apple on 30.09.22.
//

import UIKit

protocol CoordinatorContainer {
    associatedtype Screen: Routable
    
    var router: Router<Screen> { get }
    func makeController(screen: Screen) -> UIViewController
}

class AnyCoordinatorContainer<Screen: Routable>: CoordinatorContainer {
    let router: Router<Screen>
    private let makeControllerFromContainer: (Screen) -> UIViewController
    
    init(
        router: Router<Screen>,
        makeController: @escaping (Screen) -> UIViewController
    ) {
        self.router = router
        self.makeControllerFromContainer = makeController
    }
    
    func makeController(screen: Screen) -> UIViewController {
        makeControllerFromContainer(screen)
    }
}

extension CoordinatorContainer {
    func eraseToAnyCoordinatorContainer() -> AnyCoordinatorContainer<Self.Screen> {
        AnyCoordinatorContainer(router: router, makeController: makeController)
    }
}
