//
//  CoordinatorAction.swift
//  Architecture
//
//  Created by Apple on 27.09.22.
//

enum NavigationAction: String {
    case present
    case dismiss
    case push
    case pop
    case popToRoot
    
    var reversed: Self? {
        switch self {
        case .present:
            return .dismiss
        case .push:
            return .pop
        default:
            return nil
        }
    }
}
