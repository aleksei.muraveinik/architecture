//
//  Router.swift
//  Architecture
//
//  Created by Apple on 27.09.22.
//

import Combine
import Foundation

class Router<Screen: Routable> {
    
    // MARK: Properties
    private (set) var routesPublisher: AnyPublisher<[Route<Screen>], Never>!
    
    private (set) var targetInputData: Any?
    
    private let targetScreen = PassthroughSubject<Screen, Never>()
    private let graph: ScreenNode<Screen>
    private var head: ScreenNode<Screen>
    private let finishAction: (() -> Void)?
    
    
    // MARK: Lifecycle
    init(
        _ graph: ScreenNode<Screen>,
        finishAction: (() -> Void)? = nil
    ) {
        self.graph = graph
        self.head = graph
        self.finishAction = finishAction
        
        routesPublisher = targetScreen
            .map { screen in
                let (routes, targetNode) = self.head.findRoutes(target: screen)
                self.head = targetNode
                return routes
            }
            .filter(isValidTransition)
            .eraseToAnyPublisher()
    }
}


// MARK: Interface
extension Router {
    func go(to screen: Screen, data: Any? = nil) {
        targetInputData = data
        targetScreen.send(screen)
    }
    
    func goBack() {
        guard let reversed = head.reversedRoute else { return }
        targetScreen.send(reversed.target)
    }
    
    func finish() {
        guard let finishAction = finishAction else {
            assertionFailure("dismissAction for Router<\(Screen.self)> is nil")
            return
        }
        
        if head.screen != .root, head.action == .present {
            go(to: .root)
        }
        
        finishAction()
    }
}


// MARK: Validation
private func isValidTransition<Screen>(routes: [Route<Screen>]) -> Bool {
    if routes.isEmpty {
        assertionFailure("No routes were found")
        return false
    }
    
    if routes.map(\.isForward).filter({ $0 }).count > 1 {
        assertionFailure("Unable to navigate forwards more than one screen at a time")
        return false
    }
    
    return true
}
