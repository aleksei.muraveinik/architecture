//
//  Node.swift
//  Architecture
//
//  Created by Apple on 30.09.22.
//

class ScreenNode<Screen: Routable> {
    let screen: Screen
    let action: NavigationAction
    let children: [ScreenNode<Screen>]
    
    weak var parent: ScreenNode<Screen>?
    
    private var route: Route<Screen> {
        Route(screen, action)
    }
    
    var reversedRoute: Route<Screen>? {
        guard let parent = parent, let reversedAction = action.reversed else {
            return nil
        }

        return Route(parent.screen, reversedAction)
    }

    
    init(
        _ screen: Screen,
        _ action: NavigationAction,
        @NodeBuilder _ children: () -> [ScreenNode<Screen>] = { [] }
    ) {
        self.screen = screen
        self.action = action
        self.children = children()
        
        self.children.forEach { child in
            child.parent = self
        }
    }
    
    convenience init(
        @NodeBuilder _ children: () -> [ScreenNode<Screen>]
    ) {
        self.init(.root, .dismiss, children)
    }
    
    func findRoutes(target: Screen) -> ([Route<Screen>], ScreenNode<Screen>) {
        var (routes, targetNode) = findRoutes(
            target: target,
            excludeCurrentRoute: true,
            exclude: nil
        )
        
        
        if let indexOfRoot = routes.firstIndex(where: { $0.target == .root }) {
            
            
            if let indexOfPop = routes.firstIndex(where: { $0.action == .pop }) {
                
                let slice = routes[indexOfPop...indexOfRoot]
                
                if slice.allSatisfy({ $0.action == .pop }) {
                    routes = Array(routes[0..<indexOfPop])
                        + [Route(.root, .popToRoot)]
                        + (
                            (indexOfRoot == routes.count - 1)
                                ? []
                                :  routes[(indexOfRoot + 1)..<routes.count]
                        )
                }
                
            }
        }
        
        return (routes, targetNode)
    }
    
    private func findRoutes(
        target: Screen,
        excludeCurrentRoute: Bool,
        exclude: Screen?
    ) -> ([Route<Screen>], ScreenNode<Screen>) {
        if screen == target {
            if excludeCurrentRoute {
                return ([], self)
            }
            return ([route], self)
        }
        
        for child in children {
            let (routes, targetNode) = child.findRoutes(
                target: target,
                excludeCurrentRoute: false,
                exclude: screen
            )
            
            if !routes.isEmpty {
                if excludeCurrentRoute {
                    return (routes, targetNode)
                }
                return ([route] + routes, targetNode)
            }
        }
        
        guard let parent = parent, let reversedRoute = reversedRoute, exclude != parent.screen else {
            return ([], self)
        }

        let (routes, targetNode) = parent.findRoutes(
            target: target,
            excludeCurrentRoute: true,
            exclude: screen
        )
        
        return ([reversedRoute] + routes, targetNode)
    }
    
    func find(screen: Screen) -> ScreenNode<Screen>? {
        if self.screen == screen {
            return self
        }
        
        for child in children {
            if let found = child.find(screen: screen) {
                return found
            }
        }
        
        return nil
    }
}


@resultBuilder
struct NodeBuilder {
    static func buildBlock<Screen: Routable>(_ nodes: ScreenNode<Screen>...) -> [ScreenNode<Screen>] {
        nodes
    }
}

class ScreenRoot<Screen: Routable>: ScreenNode<Screen> {
    init(@NodeBuilder _ children: () -> [ScreenNode<Screen>] = { [] }) {
        super.init(.root, .present, children)
    }
}

class ScreenPresent<Screen: Routable>: ScreenNode<Screen> {
    init(_ screen: Screen, @NodeBuilder _ children: () -> [ScreenNode<Screen>] = { [] }) {
        super.init(screen, .present, children)
    }
}

class ScreenPush<Screen: Routable>: ScreenNode<Screen> {
    init(_ screen: Screen, @NodeBuilder _ children: () -> [ScreenNode<Screen>] = { [] }) {
        super.init(screen, .push, children)
    }
}
