//
//  DeeplinkService.swift
//  Architecture
//
//  Created by Apple on 06.10.22.
//

protocol DeeplinkService {
    func navigateIfNeeded()
}

final class DeeplinkServiceImpl<Screen: Routable>: DeeplinkService {
    private var targetScreen: Screen = .root
    private let router: Router<Screen>
    private var isLaunched = false
    
    init(router: Router<Screen>) {
        self.router = router
    }
    
    func setTarget(_ target: Screen) {
        targetScreen = target
        
        if isLaunched {
            router.go(to: target)
        }
    }
    
    func navigateIfNeeded() {
        defer { isLaunched = true }
        
        guard targetScreen != .root else { return }
        router.go(to: targetScreen)
    }
}
