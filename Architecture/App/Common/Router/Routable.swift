//
//  Rootable.swift
//  Architecture
//
//  Created by Apple on 29.09.22.
//

protocol Routable: Hashable {
    typealias Node = ScreenNode<Self>
    
    typealias Root = ScreenRoot<Self>
    typealias Present = ScreenPresent<Self>
    typealias Push = ScreenPush<Self>
    
    typealias Graph = Root
    
    static var root: Self { get }
    static var graph: Graph { get }
}
