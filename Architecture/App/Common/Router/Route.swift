//
//  Route.swift
//  Architecture
//
//  Created by Apple on 30.09.22.
//

struct Route<Screen: Equatable>: Equatable {
    let target: Screen
    let action: NavigationAction
    
    var isForward: Bool {
        action == .present || action == .push
    }
    
    init(_ target: Screen, _ action: NavigationAction) {
        self.target = target
        self.action = action
    }
}
