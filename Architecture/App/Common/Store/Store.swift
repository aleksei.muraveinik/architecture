//
//  Store.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import Combine
import Foundation

class Store<State, Action, OutwardAction>: ObservableObject {
    
    
    // MARK: Typealiases
    typealias State = State
    
    typealias OutwardActionHandler = (OutwardAction) -> Void
    typealias Reduce = (Action, inout State) -> OutwardAction?
    
    
    // MARK: Properties
    @Published private (set) var state: State
    
    private let reduce: Reduce
    
    
    // MARK: Lifecycle
    init(
        state: State,
        reduce: @escaping Reduce
    ) {
        self.state = state
        self.reduce = reduce
    }
    
    func processOutwardAction(_ action: OutwardAction) {
        fatalError("not implemented")
    }
}


// MARK: Interface
extension Store {
    func dispatch(_ action: Action) {
        Task { @MainActor in
            guard let outwardAction = reduce(action, &state) else { return }
            processOutwardAction(outwardAction)
        }
    }
}
