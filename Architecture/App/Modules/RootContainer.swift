//
//  RootContainer.swift
//  Architecture
//
//  Created by Apple on 27.09.22.
//

import UIKit
import SwiftUI

final class RootContainer: CoordinatorContainer {
    let router: Router<RootScreen>
    private let deeplinkService: DeeplinkService
    
    init(
        router: Router<RootScreen>,
        deeplinkService: DeeplinkService
    ) {
        self.router = router
        self.deeplinkService = deeplinkService
    }
    
    func makeController(screen: RootScreen) -> UIViewController {
        switch screen {
        case .root:
            return makeRootController()
        case .auth:
            return makeAuthController()
        case .main:
            return makeMainController()
        }
    }
}


// MARK: Private
private extension RootContainer {
    func makeRootController() -> UIViewController {
        RootView(
            viewModel: RootViewModel(
                router: router,
                deeplinkService: deeplinkService
            )
        )
        .controller
    }
    
    func makeAuthController() -> UIViewController {
        AuthView(
            viewModel: AuthViewModel(router: router)
        )
        .controller
    }
    
    func makeMainController() -> UIViewController {
        Coordinator<MainScreen>(
            container: MainContainer(
                router: Router(MainScreen.graph) { [router] in
                    router.goBack()
                }
            )
            .eraseToAnyCoordinatorContainer()
        )
        
    }
}
