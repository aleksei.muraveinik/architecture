//
//  RootCoordinator.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

enum RootScreen: Routable {
    case root
    case auth
    case main
    
    static var graph: Graph {
        Root {
            Push(.main)
            Present(.auth)
        }
    }
}
