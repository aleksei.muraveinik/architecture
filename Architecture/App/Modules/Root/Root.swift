//
//  Root.swift
//  Architecture
//
//  Created by Apple on 29.09.22.
//

struct RootState {
    var didAppearIsCalled: Bool
    
    static let initial = RootState(didAppearIsCalled: false)
}

enum RootAction {
    case didTapAuth
    case didTapMain
    case didAppear
}

enum RootOutwardAction {
    case showAuth
    case showMain
    case showFromDeeplinkIfNeeded
}

enum Root {
    static func reduce(_ action: RootAction, into state: inout RootState) -> RootOutwardAction? {
        switch action {
        case .didTapAuth:
            return .showAuth
        case .didTapMain:
            return .showMain
        case .didAppear where !state.didAppearIsCalled:
            state.didAppearIsCalled = true
            return .showFromDeeplinkIfNeeded
        case .didAppear:
            return nil
        }
    }
}
