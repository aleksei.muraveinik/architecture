//
//  RootView.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import SwiftUI

struct RootView: View {
    @ObservedObject var viewModel: RootViewModel
    
    var body: some View {
        VStack(spacing: 60) {
            Text("Root")
            
            Button("Main", action: showMain)
            
            Button("Auth", action: showAuth)
        }
        .onAppear(perform: onAppear)
    }
}


// MARK: dispatch
private extension RootView {
    func showAuth() {
        viewModel.dispatch(.didTapAuth)
    }
    
    func showMain() {
        viewModel.dispatch(.didTapMain)
    }
    
    func onAppear() {
        viewModel.dispatch(.didAppear)
    }
}
