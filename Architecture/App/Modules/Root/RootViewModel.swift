//
//  RootViewModel.swift
//  Architecture
//
//  Created by Apple on 29.09.22.
//

import Foundation

final class RootViewModel: Store<RootState, RootAction, RootOutwardAction> {
    
    private let router: Router<RootScreen>
    private let deeplinkService: DeeplinkService
    
    
    // MARK: Lifecycle
    init(
        state: State = .initial,
        router: Router<RootScreen>,
        deeplinkService: DeeplinkService
    ) {
        self.router = router
        self.deeplinkService = deeplinkService
        super.init(state: state, reduce: Root.reduce)
    }
    
    override func processOutwardAction(_ action: RootOutwardAction) {
        switch action {
            case .showAuth:
                router.go(to: .auth)
                
            case .showMain:
                router.go(to: .main)
                
            case .showFromDeeplinkIfNeeded:
                deeplinkService.navigateIfNeeded()
        }
    }
}
