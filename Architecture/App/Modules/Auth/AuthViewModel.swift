//
//  MainViewModel.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import Foundation

final class AuthViewModel {
    private let router: Router<RootScreen>
    
    init(router: Router<RootScreen>) {
        self.router = router
    }
    
    func showMain() {
        router.go(to: .main)
    }
    
    func goBack() {
        router.goBack()
    }
}
