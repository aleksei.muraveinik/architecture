//
//  AuthView.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import SwiftUI

struct AuthView: View {
    let viewModel: AuthViewModel
    
    var body: some View {
        VStack(spacing: 60) {
            Text("Auth")
            
            Button("Main", action: viewModel.showMain)
            
            Button("Back", action: viewModel.goBack)
        }
    }
}
