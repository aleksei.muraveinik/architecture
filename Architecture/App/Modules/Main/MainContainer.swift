//
//  MainContainer.swift
//  Architecture
//
//  Created by Apple on 07.10.22.
//

import UIKit

class MainContainer: CoordinatorContainer {
    let router: Router<MainScreen>
    
    init(router: Router<MainScreen>) {
        self.router = router
    }
    
    func makeController(screen: MainScreen) -> UIViewController {
        switch screen {
            case .mainPage:
                return MainView(
                    viewModel: MainViewModel(router: router)
                )
                .controller
            case .settings:
                return SettingsView(
                    viewModel: SettingsViewModel(router: router)
                )
                .controller
        }
    }
}


// MARK: Private
private extension MainContainer {
    func makeMainPageController() -> UIViewController {
        MainView(
            viewModel: MainViewModel(router: router)
        )
        .controller
    }
    
    func makeSettingsController() -> UIViewController {
        SettingsView(
            viewModel: SettingsViewModel(router: router)
        )
        .controller
    }
}
