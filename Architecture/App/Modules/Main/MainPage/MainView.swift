//
//  MainView.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import SwiftUI

struct MainView: View {
    let viewModel: MainViewModel
    
    var body: some View {
        VStack(spacing: 60) {
            Text("Main")
            
            Button("Settings", action: viewModel.showSettings)
            
            Button("Finish Coordinator<MainScreen>", action: viewModel.finishMain)
        }
    }
}
