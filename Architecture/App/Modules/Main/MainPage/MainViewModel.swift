//
//  MainViewModel.swift
//  Architecture
//
//  Created by Apple on 26.09.22.
//

import Foundation

final class MainViewModel {
    private let router: Router<MainScreen>
    
    init(router: Router<MainScreen>) {
        self.router = router
    }
    
    func finishMain() {
        router.finish()
    }
    
    func showSettings() {
        router.go(to: .settings)
    }
}
