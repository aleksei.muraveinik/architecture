//
//  SettingsView.swift
//  Architecture
//
//  Created by Apple on 07.10.22.
//

import SwiftUI

struct SettingsView: View {
    let viewModel: SettingsViewModel
    
    var body: some View {
        VStack(spacing: 60) {
            Text("Settings")
            
            Button("Back", action: viewModel.goBack)
            
            Button("Finish Coordinator<MainScreen>", action: viewModel.finishMain)
        }
    }
}
