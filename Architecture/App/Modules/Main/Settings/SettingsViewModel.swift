//
//  SettingsViewModel.swift
//  Architecture
//
//  Created by Apple on 07.10.22.
//

class SettingsViewModel {
    private let router: Router<MainScreen>
    
    init(router: Router<MainScreen>) {
        self.router = router
    }
    
    func goBack() {
        router.goBack()
    }
    
    func finishMain() {
        router.finish()
    }
}
