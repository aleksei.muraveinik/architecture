//
//  MainScreen.swift
//  Architecture
//
//  Created by Apple on 07.10.22.
//

enum MainScreen: String, Routable {
    case mainPage
    case settings
    
    static var graph: Graph {
        Root {
            Present(.settings)
        }
    }
}


// MARK: Convenience
extension MainScreen {
    static var root: MainScreen = .mainPage
}
