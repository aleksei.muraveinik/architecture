//
//  Helpers.swift
//  ArchitectureTests
//
//  Created by Apple on 30.09.22.
//

@testable import Architecture
import Foundation

struct Trajectory<Screen: Routable>: Hashable {
    let source: Screen
    let target: Screen
    
    init(_ source: Screen, _ target: Screen) {
        self.source = source
        self.target = target
    }
}


extension Routable {
    typealias FindRouteTestCases = [Trajectory<Self>: [Route<Self>]]
    typealias RouterTestCases = [Trajectory<Self>: [Route<Self>]]
}

extension Array where Element == Route<TestScreen> {
    var readable: String {
        let mapped = map {
            "(\($0.target.rawValue), \($0.action.rawValue))"
        }
        
        let joined = mapped.joined(separator: ", ")
        
        return "[\(joined)]"
    }
}
