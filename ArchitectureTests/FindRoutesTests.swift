//
//  FindRoutesTests.swift
//  ArchitectureTests
//
//  Created by Apple on 30.09.22.
//

@testable import Architecture
import XCTest

class FindRoutesTests: XCTestCase {
    
    func testRoutes() throws {
        let cases: TestScreen.FindRouteTestCases = [
            Trajectory(.a, .b): [Route(.b, .push)],
            Trajectory(.a, .c): [Route(.c, .push)],
            Trajectory(.b, .d): [Route(.d, .push)],
            Trajectory(.b, .e): [Route(.e, .present)],
            Trajectory(.b, .f): [Route(.f, .push)],
            Trajectory(.c, .h): [Route(.h, .push)],
            Trajectory(.c, .m): [Route(.m, .present)],
            Trajectory(.h, .n): [Route(.n, .present)],
            
            Trajectory(.b, .a): [Route(.a, .popToRoot)],
            Trajectory(.c, .a): [Route(.a, .popToRoot)],
            Trajectory(.d, .b): [Route(.b, .pop)],
            Trajectory(.e, .b): [Route(.b, .dismiss)],
            Trajectory(.f, .b): [Route(.b, .pop)],
            Trajectory(.h, .c): [Route(.c, .pop)],
            Trajectory(.m, .c): [Route(.c, .dismiss)],
            Trajectory(.n, .h): [Route(.h, .dismiss)],
            
            
            Trajectory(.e, .c): [
                Route(.b, .dismiss),
                Route(.a, .popToRoot),
                Route(.c, .push),
            ],
            
            Trajectory(.d, .h): [
                Route(.a, .popToRoot),
                Route(.c, .push),
                Route(.h, .push),
            ],
            
            Trajectory(.d, .c): [
                Route(.a, .popToRoot),
                Route(.c, .push),
            ],
            
            
            Trajectory(.n, .b): [
                Route(.h, .dismiss),
                Route(.a, .popToRoot),
                Route(.b, .push),
            ],
            
            Trajectory(.m, .b): [
                Route(.c, .dismiss),
                Route(.a, .popToRoot),
                Route(.b, .push),
            ],
            
            
            Trajectory(.d, .f): [
                Route(.b, .pop),
                Route(.f, .push),
            ],
            
            Trajectory(.d, .e): [
                Route(.b, .pop),
                Route(.e, .present),
            ],
            
            Trajectory(.e, .d): [
                Route(.b, .dismiss),
                Route(.d, .push),
            ],
            
            
            Trajectory(.n, .c): [
                Route(.h, .dismiss),
                Route(.c, .pop),
            ],
            
            Trajectory(.m, .h): [
                Route(.c, .dismiss),
                Route(.h, .push),
            ],
            
            Trajectory(.h, .m): [
                Route(.c, .pop),
                Route(.m, .present),
            ],
        ]
        
        try testFindRoutes(cases: cases)
    }

    private func testFindRoutes(cases: TestScreen.FindRouteTestCases) throws {
        let graph = TestScreen.graph
        
        cases.forEach { trajectory, expected in
            let sourceNode = graph.find(screen: trajectory.source)!
            let (routes, _) = sourceNode.findRoutes(target: trajectory.target)
            
            XCTAssertEqual(routes.readable, expected.readable)
        }
    }

}


// MARK: TestScreen
enum TestScreen: String, Routable {
    case a
    case b
    case c
    case d
    case e
    case f
    case h
    case m
    case n
    
    static var root = TestScreen.a
    
    static var graph: Graph {
        Root {
            Push(.b) {
                Push(.d)
                Present(.e)
                Push(.f)
            }
            
            Push(.c) {
                Push(.h) {
                    Present(.n)
                }
                Present(.m)
            }
        }
    }
    
    typealias FindRouteTestCases = [Trajectory<Self>: [Route<Self>]]
}
